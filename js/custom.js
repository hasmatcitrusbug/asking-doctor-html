

/* Set navigation */

function openNav() {
  $("#mySidenav").addClass("width80");
  $("#nav-res").addClass("opacityon");
  $(".cd-shadow-layer").addClass("displayblock");

  // document.getElementById("mySidenav").style.width = "80%";
  // document.getElementById("nav-res").style.opacity = "1";
  // document.getElementById("cd-shadow-layer").style.display = "block";
  
}

function closeNav() {
  // document.getElementById("mySidenav").style.width = "0";
  // document.getElementById("nav-res").style.opacity = "0";
  // document.getElementById("cd-shadow-layer").style.display = "none";  

  $("#mySidenav").removeClass("width80");
  $("#nav-res").removeClass("opacityon");
  $(".cd-shadow-layer").removeClass("displayblock");

} 


$(document).ready(function(){ 

  
  $(".cd-shadow-layer").click(function(){
    closeNav(); 
  });

  $('#email_allowed').change(function () {
    $('.email_div').fadeToggle();
    $('#cancel_email').click(function () { 
      $('.email_div').hide();
      $("#checkbox_email"). prop("checked", false);
    });
  });

  $('#change_password_link').click(function () {
    $('.password_div').fadeToggle();
    $('.pass_form_div').hide();
    $('#cancel_password').click(function () { 
      $('.pass_form_div').show();
      $('.password_div').hide();
    });
  });

  $('#notifications_allowed').change(function () {
    $('.notifications_div').fadeToggle();
    $('#cancel_notifications').click(function () { 
      $('.notifications_div').hide();
      $("#checkbox_notifications"). prop("checked", false);
    });
  });

  /*
  i = 1;
  $(".user_input").keypress(function(i){
    $(".username_div").show(i);
    $('#cancel_username').click(function () { 
      $('.username_div').hide();
      $(".user_input").val('');
    });
    
  });*/

  $('.user_input').keyup(function() {
    if ($(this).val().length == 0) {
      $('.username_div').hide();
    } else {
      $('.username_div').show();
    }
  }).keyup();
  $('#cancel_username').click(function () { 
    $('.username_div').hide();
    $(".user_input").val('');
  });
  

});

/* end of navigation */

